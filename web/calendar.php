<?php

require __DIR__ . "/../vendor/autoload.php";
use \SimpleEws\GetCalendar;

if (!isset($_GET["room"]) || !preg_match("/^[A-Za-z0-9_.\-]+$/", $_GET["room"])) {
    header("HTTP/1.1 400 Bad Request");
    exit;
}

$conf = parse_ini_file(__DIR__ . "/../conf/calendar.ini");
$ewsConnection = new GetCalendar($conf["host"], $conf["version"], $conf["username"], $conf["password"]);
$calendar = $ewsConnection->query(sprintf($conf["address_template"], $_GET["room"]));

if ($calendar === NULL) {
    header("HTTP/1.1 404 Not Found");
    exit;
}

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");

echo json_encode(array(
    "location" => $calendar->name,
    "event" => array_map(
        function($event) {
            return array(
                "subject" => $event->name,
                "starttime" => date("g:i A", $event->start),
                "endtime" => date("g:i A", $event->end),
                "isostarttime" => $event->start,
                "isoendtime" => $event->end,
                "organizer" => $event->organizer,
            );
        },
        $calendar->events
    ),
));
