<?php

namespace SimpleEws;

/**
 * A simple representation of a calendar event.
 */
class Event {

    /**
     * The name of the person who scheduled the event.
     * @var string
     */
    public $organizer;

    /**
     * The event's title (Outlook subject).
     * @var string
     */
    public $name;

    /**
     * Start time as a Unix timestamp.
     * @var int
     */
    public $start;

    /**
     * End time as a Unix timestamp.
     * @var int
     */
    public $end;

    /**
     * @param string $organizer The name of the person who scheduled the event.
     * @param string $name The event's title.
     * @param int $start Start time as a Unix timestamp.
     * @param int $end End time as a Unix timestamp.
     */
    public function __construct($organizer, $name, $start, $end) {
        $this->organizer = $organizer;
        $this->name = $name;
        $this->start = $start;
        $this->end = $end;
    }
}
