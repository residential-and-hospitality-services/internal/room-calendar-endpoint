<?php

namespace SimpleEws;

use \jamesiarmes\PhpEws\Client;
use \jamesiarmes\PhpEws\Enumeration\ItemQueryTraversalType;
use \jamesiarmes\PhpEws\Enumeration\DefaultShapeNamesType;
use \jamesiarmes\PhpEws\Enumeration\DistinguishedFolderIdNameType;
use \jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseFolderIdsType;
use \jamesiarmes\PhpEws\Type\ItemResponseShapeType;
use \jamesiarmes\PhpEws\Type\CalendarViewType;
use \jamesiarmes\PhpEws\Type\DistinguishedFolderIdType;
use \jamesiarmes\PhpEws\Type\EmailAddressType;
use \jamesiarmes\PhpEws\Request\FindItemType;

class GetCalendar {

    /**
     * @param string $host
     * @param string $version
     * @param string $username
     * @param string $password
     */
    public function __construct($host, $version, $username, $password) {
        $this->ewsHost = $host;
        $this->ewsVersion = $version;
        $this->ewsUsername = $username;
        $this->ewsPassword = $password;
    }

    /**
     * @param string $email
     * @param int|null $start
     * @param int|null $end
     * @return \SimpleEws\Calendar|null
     */
    public function query($email, $start = null, $end = null) {
        if (is_null($start)) {
            $start = strtotime("today");
        }
        if (is_null($end)) {
            $end = strtotime("tomorrow");
        }

        $client = new Client($this->ewsHost, $this->ewsUsername, $this->ewsPassword, $this->ewsVersion);

        $request = new FindItemType();
        $request->Traversal = ItemQueryTraversalType::SHALLOW;
        $request->ItemShape = new ItemResponseShapeType();
        $request->ItemShape->BaseShape = DefaultShapeNamesType::DEFAULT_PROPERTIES;
        $request->CalendarView = new CalendarViewType();
        $request->CalendarView->StartDate = date(DATE_ATOM, $start);
        $request->CalendarView->EndDate = date(DATE_ATOM, $end);
        $request->ParentFolderIds = new NonEmptyArrayOfBaseFolderIdsType();
        $request->ParentFolderIds->DistinguishedFolderId = new DistinguishedFolderIdType();
        $request->ParentFolderIds->DistinguishedFolderId->Id = DistinguishedFolderIdNameType::CALENDAR;
        $request->ParentFolderIds->DistinguishedFolderId->Mailbox = new EmailAddressType();
        $request->ParentFolderIds->DistinguishedFolderId->Mailbox->EmailAddress = $email;

        /* Rudimentary error handling. */
        if ($client->FindItem($request)->ResponseMessages->FindItemResponseMessage[0]->RootFolder === NULL) {
            return NULL;
        }

        $items = $client->FindItem($request)->ResponseMessages->FindItemResponseMessage[0]->RootFolder->Items
            ->CalendarItem;
        $name = null;
        $events = array();

        foreach ($items as $item) {
            if (!strlen($item->Subject)) {
                continue;
            }

            if ($name == null) {
                $name = $item->Location;
            }

            $events[] = new Event(
                $item->Organizer->Mailbox->Name,
                $item->Subject,
                strtotime($item->Start),
                strtotime($item->End)
            );
        }

        return new Calendar($name, $events);
    }
}
