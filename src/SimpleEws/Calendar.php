<?php

namespace SimpleEws;

class Calendar {

    /**
     * The calendar's name, e.g., "1855 Place Office Meeting Room 2012".
     * @var string
     */
    public $name;

    /**
     * An array of events.
     * @var Event[]
     */
    public $events = array();

    /**
     * @param string $name The calendar's name.
     * @param Event[] $events The calendar's events.
     */
    public function __construct($name, array $events) {
        $this->name = $name;
        $this->events = $events;
    }
}
